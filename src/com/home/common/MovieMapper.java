package com.home.common;

import java.util.List;

import com.home.movie.model.dto.MovieDTO;
import com.home.movie.model.dto.SearchCriteria;

public interface MovieMapper {

	List<MovieDTO> selectAllMenu();

	List<MovieDTO> searchMovie(SearchCriteria searchCriteria);

	int insertMovie(MovieDTO movie);

	int updateMovie(MovieDTO movie);

	int deleteMovie(int code);

}
