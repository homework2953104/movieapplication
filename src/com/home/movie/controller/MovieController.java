package com.home.movie.controller;

import java.util.List;
import java.util.Map;

import com.home.movie.model.dto.MovieDTO;
import com.home.movie.model.dto.SearchCriteria;
import com.home.movie.model.service.MovieService;
import com.home.movie.view.MovieResultView;

public class MovieController {

	private MovieResultView movieResultView = new MovieResultView();
	private MovieService movieService = new MovieService();
	
	
	public void selectAllMovie() {
		
		List<MovieDTO> movieList = movieService.selectAllMovies();
		
		if(movieList != null) {
				movieResultView.printMovieList(movieList);
		} else {
				movieResultView.printErrorMessage("selectList");
		}
		
		
	}


	public void searchMovie(SearchCriteria searchCriteria) {
		
		List<MovieDTO> movieList = movieService.searchMovie(searchCriteria);
		
		if(movieList != null && !movieList.isEmpty()) {
			movieResultView.printMovieList(movieList);
		} else {
			movieResultView.printErrorMessage("searchMovie");
		}
	}


	public void registMovie(Map<String, String> parameter) {
		
		MovieDTO movie = new MovieDTO();
		movie.setMovieName(parameter.get("movieName"));
		movie.setDirectorName(parameter.get("directorName"));
		movie.setGenreCode(Integer.parseInt(parameter.get("genreCode")));
		
		if(movieService.registMovie(movie)) {
			movieResultView.printSuccessMessage("insert");
		} else {
			movieResultView.printErrorMessage("insert");
		}
		
	}


	public void modifyMovie(Map<String, String> parameter) {
		
		MovieDTO movie = new MovieDTO();
		movie.setMovieCode(Integer.parseInt(parameter.get("code")));
		movie.setMovieName(parameter.get("movieName"));
		movie.setDirectorName(parameter.get("directorName"));
		movie.setGenreCode(Integer.parseInt(parameter.get("genreCode")));
		
		if(movieService.modifyMovie(movie)) {
			movieResultView.printSuccessMessage("update");	
		} else {
			movieResultView.printErrorMessage("update");
		}
	}


	public void deleteMovie(Map<String, String> parameter) {
		
		int code = Integer.parseInt(parameter.get("code"));
		
		if(movieService.deleteMovie(code)) {
			movieResultView.printSuccessMessage("delete");
		} else {
			movieResultView.printErrorMessage("delete");
		}
		
	}

}
