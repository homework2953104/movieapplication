package com.home.movie.model.dto;

public class GenreDTO {

	private int genreCode;
	private String genreName;
	
	public GenreDTO() {
		super();
	}

	public GenreDTO(int genreCode, String genreName) {
		super();
		this.genreCode = genreCode;
		this.genreName = genreName;
	}

	public int getGenreCode() {
		return genreCode;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreCode(int genreCode) {
		this.genreCode = genreCode;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	@Override
	public String toString() {
		return "GenreDTO [genreCode=" + genreCode + ", genreName=" + genreName + "]";
	}
	
	
}
