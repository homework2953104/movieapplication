package com.home.movie.model.dto;

public class MovieDTO {

	private int movieCode;
	private String movieName;
	private String directorName;
	private int genreCode;
	private String viewableStatus;
	
	public MovieDTO() {
		super();
	}

	public MovieDTO(int movieCode, String movieName, String directorName, int genreCode, String viewableStatus) {
		super();
		this.movieCode = movieCode;
		this.movieName = movieName;
		this.directorName = directorName;
		this.genreCode = genreCode;
		this.viewableStatus = viewableStatus;
	}

	public int getMovieCode() {
		return movieCode;
	}

	public String getMovieName() {
		return movieName;
	}

	public String getDirectorName() {
		return directorName;
	}

	public int getGenreCode() {
		return genreCode;
	}

	public String getViewableStatus() {
		return viewableStatus;
	}

	public void setMovieCode(int movieCode) {
		this.movieCode = movieCode;
	}

	public void setMovieName(String movieName) {
		this.movieName = movieName;
	}

	public void setDirectorName(String directorName) {
		this.directorName = directorName;
	}

	public void setGenreCode(int genreCode) {
		this.genreCode = genreCode;
	}

	public void setViewableStatus(String viewableStatus) {
		this.viewableStatus = viewableStatus;
	}

	@Override
	public String toString() {
		return "MovieDTO [movieCode=" + movieCode + ", movieName=" + movieName + ", directorName=" + directorName
				+ ", genreCode=" + genreCode + ", viewableStatus=" + viewableStatus + "]";
	}
	
	
}