package com.home.movie.model.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.home.common.MovieMapper;
import com.home.movie.model.dto.MovieDTO;
import com.home.movie.model.dto.SearchCriteria;

import static com.home.common.Template.getSqlSession;

public class MovieService {

	private MovieMapper movieMapper;
	
	public List<MovieDTO> selectAllMovies() {
		
		SqlSession sqlSession = getSqlSession();
		movieMapper = sqlSession.getMapper(MovieMapper.class);
		
		List<MovieDTO> movieList = movieMapper.selectAllMenu();
		
		sqlSession.close();
		
		return movieList;
	}

	public List<MovieDTO> searchMovie(SearchCriteria searchCriteria) {
		
		SqlSession sqlSession = getSqlSession();
		movieMapper = sqlSession.getMapper(MovieMapper.class);
		
		List<MovieDTO> movieList = movieMapper.searchMovie(searchCriteria);
		
		sqlSession.close();
		
		return movieList;
	}

	public boolean registMovie(MovieDTO movie) {
		
		SqlSession sqlSession = getSqlSession();
		movieMapper = sqlSession.getMapper(MovieMapper.class);
		
		int result = movieMapper.insertMovie(movie);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean modifyMovie(MovieDTO movie) {
		
		SqlSession sqlSession = getSqlSession();
		movieMapper = sqlSession.getMapper(MovieMapper.class);
		
		int result = movieMapper.updateMovie(movie);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean deleteMovie(int code) {
		
		SqlSession sqlSession = getSqlSession();
		movieMapper = sqlSession.getMapper(MovieMapper.class);
		
		int result = movieMapper.deleteMovie(code);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true: false;
	}

}
