package com.home.movie.view;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.home.movie.controller.MovieController;
import com.home.movie.model.dto.SearchCriteria;

public class MovieMenu {

	private Scanner sc = new Scanner(System.in);
	
	public void displayMenu() {
		
		MovieController movieController = new MovieController();
		
		do {
			System.out.println("===== 영화 메뉴 관리 프로그램 =====");
			System.out.println("1. 영화 전체 조회");
			System.out.println("2. 영화 이름 혹은 장르로 검색하여 조회");
			System.out.println("3. 신규 영화 추가");
			System.out.println("4. 영화 정보 수정");
			System.out.println("5. 영화 정보 삭제");
			System.out.println("9. 프로그램 종료");
			System.out.print("메뉴 관리 번호를 입력 : ");
			int no = sc.nextInt();
			
			switch(no) {
			case 1: movieController.selectAllMovie(); break;
			case 2: movieController.searchMovie(inputSearchCriteria()); break;
			case 3: movieController.registMovie(inputMovie()); break;
			case 4: movieController.modifyMovie(inputModifyMovie()); break;
			case 5: movieController.deleteMovie(inputMovieCode()); break;
			case 9: return;
			default : System.out.println("잘못된 번호입니다. 다시 입력해주세요.");
			}
			
		} while(true);
		
		
		
	}

	private static Map<String, String> inputMovieCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("영화 코드 입력 : ");
		String code = sc.nextLine();
		
		Map <String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		
		return parameter;
	}

	private static Map<String, String> inputModifyMovie() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("수정할 영화 코드 : ");
		String code = sc.nextLine();
		System.out.print("수정할 영화 이름 : ");
		String movieName = sc.nextLine();
		System.out.print("수정할 감독 이름 : ");
		String directorName = sc.nextLine();
		System.out.print("수정할 장르 코드 : ");
		String genreCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		parameter.put("movieName", movieName);
		parameter.put("directorName", directorName);
		parameter.put("genreCode", genreCode);
		
		return parameter;
	}

	private static Map<String, String> inputMovie() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("영화 이름 : ");
		String movieName = sc.nextLine();
		System.out.print("감독 이름 : ");
		String directorName = sc.nextLine();
		System.out.print("장르 코드 : ");
		String genreCode = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("movieName", movieName);
		parameter.put("directorName", directorName);
		parameter.put("genreCode", genreCode);
		
		return parameter;
	}

	private SearchCriteria inputSearchCriteria() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("검색 기준을 입력해주세요(name or genre) : ");
		String condition = sc.nextLine();
		System.out.print("검색어를 입력해주세요 : ");
		String value = sc.nextLine();
		
		return new SearchCriteria(condition, value);
	}

}
