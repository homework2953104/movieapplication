package com.home.movie.view;

import java.util.List;

import com.home.movie.model.dto.MovieDTO;

public class MovieResultView {

	public void printMovieList(List<MovieDTO> movieList) {
		
		for(MovieDTO movie : movieList) {
			System.out.println(movie);
		}
		
	}

	public void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		case "selectList": errorMessage = "영화 목록 조회에 실패했습니다."; break;
		case "searchMoive" : errorMessage = "영화이름 혹은 영화 감독명 조회에 실패했습니다."; break;
		case "insert" : errorMessage = "영화 등록에 실패했습니다."; break;
		case "update" : errorMessage = "영화 수정에 실패했습니다."; break;
		case "delete" : errorMessage = "영화 삭제에 실패했습니다."; break;
		}
		
		System.out.println(errorMessage);
	}

	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		switch(successCode) {
		case "insert" : successMessage = "영화 등록에 성공하였습니다."; break;
		case "update" : successMessage = "영화 수정에 성공하였습니다."; break;
		case "delete" : successMessage = "영화 삭제에 성공하였습니다."; break;
		}
		
		System.out.println(successMessage);
	}


	
}
