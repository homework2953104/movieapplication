package com.home.run;

import com.home.movie.view.MovieMenu;

public class Application {

	public static void main(String[] args) {
	
		new MovieMenu().displayMenu();
		
		System.out.println("영화 관리 프로그램을 종료합니다.");

	}

}
